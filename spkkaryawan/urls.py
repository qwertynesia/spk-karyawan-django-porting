from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.conf import settings

from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
                       # Examples:
                       # url(r'^$', 'spkkaryawan.views.home', name='home'),
                       # url(r'^blog/', include('blog.urls')),

                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^$', 'spk.views.login'),
                       url(r'^logout/$', 'spk.views.logout', name='keluar'),
                       url(r'^401/$', 'spk.views.halaman_terlarang'),
                       url(r'^main/$', 'spk.views.main_page', name='menu-main'),
                       url(r'^unit/$', 'spk.views.menu_unit_kerja', name='menu-unit-kerja'),
                       url(r'^unit/sunting/(\d+)/$', 'spk.views.menu_unit_kerja_sunting', name='perbarui-unit-kerja'),
                       url(r'^unit/tambah/', 'spk.views.menu_unit_kerja_tambah', name='tambah-unit-kerja'),
                       url(r'^unit/hapus/(\d+)/$', 'spk.views.menu_unit_kerja_hapus', name='hapus-unit-kerja'),
                       url(r'^karyawan/$', 'spk.views.menu_karyawan', name='menu-karyawan'),
                       url(r'^karyawan/tambah/', 'spk.views.menu_karyawan_tambah', name='tambah-karyawan'),
                       url(r'^karyawan/sunting/(\d+)/$', 'spk.views.menu_karyawan_sunting', name='perbarui-karyawan'),
                       url(r'^karyawan/hapus/(\d+)/$', 'spk.views.menu_karyawan_hapus', name='hapus-karyawan'),
                       url(r'^kriteria/$', 'spk.views.menu_kriteria', name='menu-kriteria'),
                       url(r'^kriteria/sunting/(\d+)/$', 'spk.views.menu_kriteria_sunting', name='perbarui-kriteria'),
                       url(r'^pembobotan/$', 'spk.views.menu_pembobotan_ahp', name='menu-pembobotan'),
                       url(r'^pembobotan/hapus/$', 'spk.views.menu_hapus_pembobotan_ahp', name='hapus-bobot'),
                       url(r'^perangkingan/$', 'spk.views.menu_perangkingan_promete', name='menu-perangkingan'),
                       url(r'^perangkingan/unit/(\d+)/$', 'spk.views.json_unit_kerja'),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
