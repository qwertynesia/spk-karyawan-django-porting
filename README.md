# README #

Sistem pendukung keputusan dengan metode AHP dan Promethee merupakan project lama saya yang saya rewrite ulang dalam rangka belajar Django framework dan menggunakan database MySQL.

### Get Started ###
Disarankan menggunakan virtualenv untuk menjalankan aplikasi ini, dan install semua dependensi Django melalui pip. Pada saat aplikasi ini dibangun menggunakan Django versi 1.6.5

    (venv) $pip install django
	(venv) $pip install mysql-python
	(venv) $pip install Pillow

Bagi pengguna windows untuk install mysql-python bisa menggunakan alternatif ini.
Download executable MySQLdb di [sini](http://www.lfd.uci.edu/~gohlke/pythonlibs/#mysql-python) , sesuaikan dengan versi python.
install menggunakan easy_install dalam keadaan virtualenv aktif.

    (venv) $easy_install c:\mydata\MySQL-python-1.2.5.win32-py2.7.exe

path mysql-python sesuaikan dengan komputer masing-masing. Jalankan perintah berikut untuk menjalankan project pertama kali. Untuk selanjutnya cukup dengan runserver.

    (venv) $python manage.py syncdb
	(venv) $python manage.py runserver 