from django.forms import ModelForm, TextInput
from django.forms.widgets import Select

from spk.models import *


class UnitKerjaForm(ModelForm):
    class Meta:
        model = UnitKerja
        fields = ['nama_unit_kerja']
        widgets = {
            'nama_unit_kerja': TextInput(attrs={
                'Placeholder': 'Nama unit kerja',
                'style': 'height:40px;',
                'class': 'span6 disabled'
            })
        }


class KaryawanForm(ModelForm):
    class Meta:
        model = Karyawan
        fields = '__all__'
        widgets = {
            'nik': TextInput(attrs={
                'Placeholder': 'NIK Karyawan',
                'style': 'height:40px;',
                'class': 'span6 disabled',
                'readonly': 'true'
            }),
            'nama': TextInput(attrs={
                'Placeholder': 'Nama Karyawan',
                'style': 'height:40px;',
                'class': 'span6 disabled'
            }),
            'tanggal_masuk': TextInput(attrs={
                'Placeholder': 'Tanggal Masuk',
                'style': 'height:40px;',
                'class': 'span6 disabled'
            }),
            'jenis_kelamin': Select(attrs={
                'style': 'height:40px;',
                'class': 'span6 disabled'
            }),
            'unit_kerja': Select(attrs={
                'style': 'height:40px;',
                'class': 'span6 disabled'
            }),
        }


class KriteriaForm(ModelForm):
    class Meta:
        model = Kriteria
        exclude = ['id', 'nilai_karyawan']
        widgets = {
            'nama_kriteria': TextInput(attrs={
                'style': 'height:40px;',
                'Placeholder': 'Nama Kriteria',
                'class': 'span6 disabled',
                'readonly': 'true'
            }),
            'tipe_preferensi': Select(attrs={
                'style': 'height:40px;',
                'class': 'span6 disabled'
            }),
            'max_min': Select(attrs={
                'style': 'height:40px;',
                'class': 'span6 disabled'
            }),
            'parameter_p': TextInput(attrs={
                'style': 'height:40px;',
                'Placeholder': 'Nilai Parameter P',
                'class': 'span6 disabled'
            }),
            'parameter_q': TextInput(attrs={
                'style': 'height:40px;',
                'Placeholder': 'Nilai Parameter Q',
                'class': 'span6 disabled'
            }),
        }
