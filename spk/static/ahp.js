var hitungKolomMatrix = function(matrix, index) {
    var hasil = 0;
    for(var i = 0; i < matrix.length; i++) {
        hasil += parseFloat(matrix[i][index]);
    }
    // toFixed used to create 2 decimal places
    // the + sign drops the extra zeroes at the end
    return +hasil.toFixed(2);
};

var hitungBobot = function(matrix, index) {
    var hasil = 0;
    for (var i = 0; i < matrix.length; i++) {
        hasil += matrix[index][i];
    }
    return (hasil / matrix.length).toFixed(2);
}

$(document).ready(function() {


    //----------baris pengetahuan mesin----------
    $("select[name='penget_mesin0'], select[name='penget_mesin1']").change(function() {
        var nilai = $("select[name='penget_mesin0']").val() / $("select[name='penget_mesin1']").val();
        $("input[name='kondi_psikis0']").val(nilai);
    });

    $("select[name='penget_mesin0'], select[name='penget_mesin2']").change(function() {
        var nilai = $("select[name='penget_mesin0']").val() / $("select[name='penget_mesin2']").val();
        $("input[name='kondi_mesin0']").val(nilai);
    });

    $("select[name='penget_mesin0'], select[name='penget_mesin3']").change(function() {
        var nilai = $("select[name='penget_mesin0']").val() / $("select[name='penget_mesin3']").val();
        $("input[name='kondi_lingku0']").val(nilai);
    });

    $("select[name='penget_mesin0'], select[name='penget_mesin4']").change(function() {
        var nilai = $("select[name='penget_mesin0']").val() / $("select[name='penget_mesin4']").val();
        $("input[name='kondi_fisik0']").val(nilai);
    });

    $("select[name='penget_mesin0'], select[name='penget_mesin5']").change(function() {
        var nilai = $("select[name='penget_mesin0']").val() / $("select[name='penget_mesin5']").val();
        $("input[name='masa_kerja0']").val(nilai);
    });

    $("select[name='penget_mesin0'], select[name='penget_mesin6']").change(function() {
        var nilai = $("select[name='penget_mesin0']").val() / $("select[name='penget_mesin6']").val();
        $("input[name='pendi_kary0']").val(nilai);
    });

    $("select[name='penget_mesin0'], select[name='penget_mesin7']").change(function() {
        var nilai = $("select[name='penget_mesin0']").val() / $("select[name='penget_mesin7']").val();
        $("input[name='usia0']").val(nilai);
    });

    //----------baris kondisi psikis----------
    $("select[name='kondi_psikis1'], select[name='kondi_psikis2']").change(function() {
        var nilai = $("select[name='kondi_psikis1']").val() / $("select[name='kondi_psikis2']").val();
        $("input[name='kondi_mesin1']").val(nilai);
    });

    $("select[name='kondi_psikis1'], select[name='kondi_psikis3']").change(function() {
        var nilai = $("select[name='kondi_psikis1']").val() / $("select[name='kondi_psikis3']").val();
        $("input[name='kondi_lingku1']").val(nilai);
    });

    $("select[name='kondi_psikis1'], select[name='kondi_psikis4']").change(function() {
        var nilai = $("select[name='kondi_psikis1']").val() / $("select[name='kondi_psikis4']").val();
        $("input[name='kondi_fisik1']").val(nilai);
    });

    $("select[name='kondi_psikis1'], select[name='kondi_psikis5']").change(function() {
        var nilai = $("select[name='kondi_psikis1']").val() / $("select[name='kondi_psikis5']").val();
        $("input[name='masa_kerja1']").val(nilai);
    });

    $("select[name='kondi_psikis1'], select[name='kondi_psikis6']").change(function() {
        var nilai = $("select[name='kondi_psikis1']").val() / $("select[name='kondi_psikis6']").val();
        $("input[name='pendi_kary1']").val(nilai);
    });

    $("select[name='kondi_psikis1'], select[name='kondi_psikis7']").change(function() {
        var nilai = $("select[name='kondi_psikis1']").val() / $("select[name='kondi_psikis7']").val();
        $("input[name='usia1']").val(nilai);
    });

    //----------baris kondisi mesin----------
    $("select[name='kondi_mesin2'], select[name='kondi_mesin3']").change(function() {
        var nilai = $("select[name='kondi_mesin2']").val() / $("select[name='kondi_mesin3']").val();
        $("input[name='kondi_lingku2']").val(nilai);
    });

    $("select[name='kondi_mesin2'], select[name='kondi_mesin4']").change(function() {
        var nilai = $("select[name='kondi_mesin2']").val() / $("select[name='kondi_mesin4']").val();
        $("input[name='kondi_fisik2']").val(nilai);
    });

    $("select[name='kondi_mesin2'], select[name='kondi_mesin5']").change(function() {
        var nilai = $("select[name='kondi_mesin2']").val() / $("select[name='kondi_mesin5']").val();
        $("input[name='masa_kerja2']").val(nilai);
    });

    $("select[name='kondi_mesin2'], select[name='kondi_mesin6']").change(function() {
        var nilai = $("select[name='kondi_mesin2']").val() / $("select[name='kondi_mesin6']").val();
        $("input[name='pendi_kary2']").val(nilai);
    });

    $("select[name='kondi_mesin2'], select[name='kondi_mesin7']").change(function() {
        var nilai = $("select[name='kondi_mesin2']").val() / $("select[name='kondi_mesin7']").val();
        $("input[name='usia2']").val(nilai);
    });

    //----------baris kondisi lingkungan----------
     $("select[name='kondi_lingku3'], select[name='kondi_lingku4']").change(function() {
        var nilai = $("select[name='kondi_lingku3']").val() / $("select[name='kondi_lingku4']").val();
        $("input[name='kondi_fisik3']").val(nilai);
    });

    $("select[name='kondi_lingku3'], select[name='kondi_lingku5']").change(function() {
        var nilai = $("select[name='kondi_lingku3']").val() / $("select[name='kondi_lingku5']").val();
        $("input[name='masa_kerja3']").val(nilai);
    });

    $("select[name='kondi_lingku3'], select[name='kondi_lingku6']").change(function() {
        var nilai = $("select[name='kondi_lingku3']").val() / $("select[name='kondi_lingku6']").val();
        $("input[name='pendi_kary3']").val(nilai);
    });

    $("select[name='kondi_lingku3'], select[name='kondi_lingku7']").change(function() {
        var nilai = $("select[name='kondi_lingku3']").val() / $("select[name='kondi_lingku7']").val();
        $("input[name='usia3']").val(nilai);
    });

    //----------baris kondisi fisik----------
    $("select[name='kondi_fisik4'], select[name='kondi_fisik5']").change(function() {
        var nilai = $("select[name='kondi_fisik4']").val() / $("select[name='kondi_fisik5']").val();
        $("input[name='masa_kerja4']").val(nilai);
    });

    $("select[name='kondi_fisik4'], select[name='kondi_fisik6']").change(function() {
        var nilai = $("select[name='kondi_fisik4']").val() / $("select[name='kondi_fisik6']").val();
        $("input[name='pendi_kary4']").val(nilai);
    });

    $("select[name='kondi_fisik4'], select[name='kondi_fisik7']").change(function() {
        var nilai = $("select[name='kondi_fisik4']").val() / $("select[name='kondi_fisik7']").val();
        $("input[name='usia4']").val(nilai);
    });

    //----------baris masa kerja----------
    $("select[name='masa_kerja5'], select[name='masa_kerja6']").change(function() {
        var nilai = $("select[name='masa_kerja5']").val() / $("select[name='masa_kerja6']").val();
        $("input[name='pendi_kary5']").val(nilai);
    });

    $("select[name='masa_kerja5'], select[name='masa_kerja7']").change(function() {
        var nilai = $("select[name='masa_kerja5']").val() / $("select[name='masa_kerja7']").val();
        $("input[name='usia5']").val(nilai);
    });

    //----------baris pendidikan karyawan----------
    $("select[name='pendi_kary6'], select[name='pendi_kary7']").change(function() {
        var nilai = $("select[name='pendi_kary6']").val() / $("select[name='pendi_kary7']").val();
        $("input[name='usia6']").val(nilai);
    });

    //inisiasi array pertama setiap perubahan dropdown
    var firstMatrix = [];
    $("select").change(function() {
        firstMatrix[0] = [
            $("select[name='penget_mesin0']").val(),
            $("select[name='penget_mesin1']").val(),
            $("select[name='penget_mesin2']").val(),
            $("select[name='penget_mesin3']").val(),
            $("select[name='penget_mesin4']").val(),
            $("select[name='penget_mesin5']").val(),
            $("select[name='penget_mesin6']").val(),
            $("select[name='penget_mesin7']").val()
        ];

        firstMatrix[1] = [
            $("input[name='kondi_psikis0']").val(),
            $("select[name='kondi_psikis1']").val(),
            $("select[name='kondi_psikis2']").val(),
            $("select[name='kondi_psikis3']").val(),
            $("select[name='kondi_psikis4']").val(),
            $("select[name='kondi_psikis5']").val(),
            $("select[name='kondi_psikis6']").val(),
            $("select[name='kondi_psikis7']").val()
        ];

        firstMatrix[2] = [
            $("input[name='kondi_mesin0']").val(),
            $("input[name='kondi_mesin1']").val(),
            $("select[name='kondi_mesin2']").val(),
            $("select[name='kondi_mesin3']").val(),
            $("select[name='kondi_mesin4']").val(),
            $("select[name='kondi_mesin5']").val(),
            $("select[name='kondi_mesin6']").val(),
            $("select[name='kondi_mesin7']").val()
        ];

        firstMatrix[3] = [
            $("input[name='kondi_lingku0']").val(),
            $("input[name='kondi_lingku1']").val(),
            $("input[name='kondi_lingku2']").val(),
            $("select[name='kondi_lingku3']").val(),
            $("select[name='kondi_lingku4']").val(),
            $("select[name='kondi_lingku5']").val(),
            $("select[name='kondi_lingku6']").val(),
            $("select[name='kondi_lingku7']").val()
        ];

        firstMatrix[4] = [
            $("input[name='kondi_fisik0']").val(),
            $("input[name='kondi_fisik1']").val(),
            $("input[name='kondi_fisik2']").val(),
            $("input[name='kondi_fisik3']").val(),
            $("select[name='kondi_fisik4']").val(),
            $("select[name='kondi_fisik5']").val(),
            $("select[name='kondi_fisik6']").val(),
            $("select[name='kondi_fisik7']").val()
        ];

        firstMatrix[5] = [
            $("input[name='masa_kerja0']").val(),
            $("input[name='masa_kerja1']").val(),
            $("input[name='masa_kerja2']").val(),
            $("input[name='masa_kerja3']").val(),
            $("input[name='masa_kerja4']").val(),
            $("select[name='masa_kerja5']").val(),
            $("select[name='masa_kerja6']").val(),
            $("select[name='masa_kerja7']").val()
        ];

        firstMatrix[6] = [
            $("input[name='pendi_kary0']").val(),
            $("input[name='pendi_kary1']").val(),
            $("input[name='pendi_kary2']").val(),
            $("input[name='pendi_kary3']").val(),
            $("input[name='pendi_kary4']").val(),
            $("input[name='pendi_kary5']").val(),
            $("select[name='pendi_kary6']").val(),
            $("select[name='pendi_kary7']").val()
        ];

        firstMatrix[7] = [
            $("input[name='usia0']").val(),
            $("input[name='usia1']").val(),
            $("input[name='usia2']").val(),
            $("input[name='usia3']").val(),
            $("input[name='usia4']").val(),
            $("input[name='usia5']").val(),
            $("input[name='usia6']").val(),
            $("select[name='usia7']").val()
        ];

        //jumlah matrix tahap I
        jumlahMatrix = [
            hitungKolomMatrix(firstMatrix, 0),
            hitungKolomMatrix(firstMatrix, 1),
            hitungKolomMatrix(firstMatrix, 2),
            hitungKolomMatrix(firstMatrix, 3),
            hitungKolomMatrix(firstMatrix, 4),
            hitungKolomMatrix(firstMatrix, 5),
            hitungKolomMatrix(firstMatrix, 6),
            hitungKolomMatrix(firstMatrix, 7),
        ]

        //set nilai total matrix
        $("input[name='jml_0']").val(jumlahMatrix[0]);
        $("input[name='jml_1']").val(jumlahMatrix[1]);
        $("input[name='jml_2']").val(jumlahMatrix[2]);
        $("input[name='jml_3']").val(jumlahMatrix[3]);
        $("input[name='jml_4']").val(jumlahMatrix[4]);
        $("input[name='jml_5']").val(jumlahMatrix[5]);
        $("input[name='jml_6']").val(jumlahMatrix[6]);
        $("input[name='jml_7']").val(jumlahMatrix[7]);

        //inisiasi normalisasi matrix
        var matrixNormal = [];
        for (var i = 0; i < 8; i++) {
            matrixNormal[i] = [];
            for (var j = 0; j < 8; j++) {
                matrixNormal[i][j] = firstMatrix[i][j] / jumlahMatrix[j];
            }
        }

        //set bobot ke form
        $("input[name='bobot_pengetahuan_mesin']").val(hitungBobot(matrixNormal, 0));
        $("input[name='bobot_kondisi_psikis']").val(hitungBobot(matrixNormal, 1));
        $("input[name='bobot_kondisi_mesin']").val(hitungBobot(matrixNormal, 2));
        $("input[name='bobot_kondisi_lingkungan']").val(hitungBobot(matrixNormal, 3));
        $("input[name='bobot_kondisi_fisik']").val(hitungBobot(matrixNormal, 4));
        $("input[name='bobot_masa_kerja']").val(hitungBobot(matrixNormal, 5));
        $("input[name='bobot_pendidikan']").val(hitungBobot(matrixNormal, 6));
        $("input[name='bobot_usia']").val(hitungBobot(matrixNormal, 7));

        //validasi button proceed jika kosong
        if ($("input[name='bobot_usia']").val() === 0) {
            $("input[value='Proceed']").prop("disabled", true);
        } else {
            $("input[value='Proceed']").prop("disabled", false);
        }
    });
});