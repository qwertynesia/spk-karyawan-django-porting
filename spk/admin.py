from django.contrib import admin
from spk.models import Pengguna

class PenggunaAdmin(admin.ModelAdmin):
    list_display = ('user_nama', 'user_username', 'user_password', 'user_level')
    search_field = ['user_nama']

admin.site.register(Pengguna, PenggunaAdmin)
