import json

from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib import messages, auth
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

import promethee as promet

from spk.forms import *


def login(request):
    if request.method == 'POST':
        if 'username' in request.POST and 'password' in request.POST:
            username = request.POST.get('username', '')
            password = request.POST.get('password', '')
            user = auth.authenticate(username=username, password=password)

            if user is not None:
                auth.login(request, user)
                return HttpResponseRedirect(reverse('spk.views.main_page'))
            else:
                messages.add_message(request, messages.ERROR, "Kombinasi username & password tidak tepat.")
                return HttpResponseRedirect(reverse('spk.views.login'))

    # redirect ke main page kalo user telah login
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('spk.views.main_page'))
    return render(request, 'login-page.html')


def logout(request):
    auth.logout(request)
    return HttpResponseRedirect(reverse('spk.views.login'))


def halaman_terlarang(request):
    return render(request, 'terlarang.html')


@login_required(login_url='/401')
def main_page(request):
    return render(request, 'main.html', {'pengguna': request.user.username})


@login_required(login_url='/401')
def menu_pembobotan_ahp(request):
    if request.method == 'POST':
        '''
        batch insert bobot pengetahuan mesin
        '''
        BobotKriteria.objects.bulk_create([
            BobotKriteria(bobot=request.POST.get('bobot_pengetahuan_mesin'), kriteria=Kriteria.objects.get(id=2)),
            BobotKriteria(bobot=request.POST.get('bobot_kondisi_psikis'), kriteria=Kriteria.objects.get(id=3)),
            BobotKriteria(bobot=request.POST.get('bobot_kondisi_mesin'), kriteria=Kriteria.objects.get(id=4)),
            BobotKriteria(bobot=request.POST.get('bobot_kondisi_lingkungan'), kriteria=Kriteria.objects.get(id=5)),
            BobotKriteria(bobot=request.POST.get('bobot_kondisi_fisik'), kriteria=Kriteria.objects.get(id=6)),
            BobotKriteria(bobot=request.POST.get('bobot_masa_kerja'), kriteria=Kriteria.objects.get(id=7)),
            BobotKriteria(bobot=request.POST.get('bobot_pendidikan'), kriteria=Kriteria.objects.get(id=8)),
            BobotKriteria(bobot=request.POST.get('bobot_usia'), kriteria=Kriteria.objects.get(id=9))
        ])

        messages.add_message(request, messages.SUCCESS, "Pembobotan kriteria berhasil.")
        return HttpResponseRedirect(reverse('spk.views.menu_pembobotan_ahp'))

    # cek apakah data bobot sudah ada isinya atau belum
    available = BobotKriteria.objects.all().exists()
    bobot = BobotKriteria.objects.values_list('bobot', flat=True)
    return render(request, 'menu-bobot-ahp.html',
                  {'dropdown_value': [x for x in range(0, 10)], 'hit': available, 'bobot': bobot})


@login_required(login_url='/401')
def menu_hapus_pembobotan_ahp(request):
    BobotKriteria.objects.all().delete()
    messages.add_message(request, messages.SUCCESS, "Bobot kriteria berhasil dihapus.")
    return HttpResponseRedirect(reverse('spk.views.menu_pembobotan_ahp'))


@login_required(login_url='/401')
def menu_kriteria(request):
    return render(request, 'menu-kriteria.html', {'kriteria': Kriteria.objects.all()})


@login_required(login_url='/401')
def menu_kriteria_sunting(request, id_kriteria):
    kriteria = Kriteria.objects.get(pk=id_kriteria)
    if request.method == 'POST':
        form = KriteriaForm(request.POST, instance=kriteria)
        if form.is_valid():
            kriteria.nama_kriteria = form.cleaned_data['nama_kriteria']
            kriteria.parameter_p = form.cleaned_data['parameter_p']
            kriteria.parameter_q = form.cleaned_data['parameter_q']
            kriteria.tipe_preferensi = form.cleaned_data['tipe_preferensi']
            kriteria.max_min = form.cleaned_data['max_min']
            kriteria.save()
            messages.add_message(request, messages.SUCCESS, 'Kriteria %s berhasil diperbarui.' % kriteria.nama_kriteria)
            return HttpResponseRedirect(reverse('spk.views.menu_kriteria'))
        else:
            return render(request, 'menu-kriteria-form.html', {'form': form})
    form = KriteriaForm(instance=kriteria)
    return render(request, 'menu-kriteria-form.html', {'form': form})


@login_required(login_url='/401')
def menu_karyawan(request):
    karyawan_list = Karyawan.objects.all()
    paginator = Paginator(karyawan_list, 5)  # display 5 karyawan per page

    page = request.GET.get('page')
    try:
        karyawans = paginator.page(page)
    except PageNotAnInteger:
        karyawans = paginator.page(1)
    except EmptyPage:
        karyawans = paginator.page(paginator.num_pages)
    return render(request, 'menu-karyawan.html', {'karyawan': karyawans})


@login_required(login_url='/401')
def menu_karyawan_tambah(request):
    if request.method == 'POST':
        form = KaryawanForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS, 'Karyawan berhasil ditambahkan.')
            return HttpResponseRedirect(reverse('spk.views.menu_karyawan'))
        else:
            return render(request, 'menu-karyawan-form.html', {'form': form, 'button': 'Tambahkan'})

    # ambil data nik terakhir
    last_nik = Karyawan.objects.values_list('nik', flat=True).last()

    # generate_nik = '1000000001' kalo data kosong, dan tambah 1 last_nik yang sudah dikonversi ke int
    generate_nik = 1000000001 if last_nik is None else int(last_nik) + 1

    form = KaryawanForm(initial={'nik': generate_nik})
    return render(request, 'menu-karyawan-form.html', {'form': form, 'button': 'Tambahkan'})


@login_required(login_url='/401')
def menu_karyawan_sunting(request, nik):
    karyawan = Karyawan.objects.get(nik=nik)
    if request.method == 'POST':
        form = KaryawanForm(request.POST, request.FILES, instance=karyawan)
        if form.is_valid():
            karyawan.nama = form.cleaned_data['nama']
            karyawan.jenis_kelamin = form.cleaned_data['jenis_kelamin']
            karyawan.tanggal_masuk = form.cleaned_data['tanggal_masuk']
            karyawan.unit_kerja = form.cleaned_data['unit_kerja']
            karyawan.save()
            messages.add_message(request, messages.SUCCESS, 'Data %s berhasil diperbarui.' % karyawan.nama)
            return HttpResponseRedirect(reverse('spk.views.menu_karyawan'))
        else:
            print form.cleaned_data['unit_kerja']
            print form.errors
            return render(request, 'menu-karyawan-form.html', {'form': form.errors, 'button': 'Perbarui'})
    form = KaryawanForm(instance=karyawan)
    return render(request, 'menu-karyawan-form.html', {'form': form, 'button': 'Perbarui'})


@login_required(login_url='/401')
def menu_karyawan_hapus(request, nik):
    Karyawan.objects.filter(nik__exact=nik).delete()
    messages.add_message(request, messages.SUCCESS, 'Data %s berhasil dihapus.' % nik)
    return HttpResponseRedirect(reverse('spk.views.menu_karyawan'))


@login_required(login_url='/401')
def menu_unit_kerja(request):
    return render(request, 'menu-unit-kerja.html', {'uker': UnitKerja.objects.all()})


@login_required(login_url='/401')
def menu_unit_kerja_tambah(request):
    if request.method == 'POST':
        form = UnitKerjaForm(request.POST)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS, "Data berhasil ditambahkan.")
            return HttpResponseRedirect(reverse('spk.views.menu_unit_kerja'))
        else:
            return render(request, 'menu-unit-kerja-form.html', {'form': form, 'button': 'Tambahkan'})
    form = UnitKerjaForm()
    return render(request, 'menu-unit-kerja-form.html', {'form': form, 'button': 'Tambahkan'})


@login_required(login_url='/401')
def menu_unit_kerja_hapus(request, id_unit):
    unitkerja = UnitKerja.objects.get(id=id_unit)
    unitkerja.delete()
    messages.add_message(request, messages.SUCCESS, "Data %s berhasil dihapus." % unitkerja.nama_unit_kerja)
    return HttpResponseRedirect(reverse('spk.views.menu_unit_kerja'))


@login_required(login_url='/401')
def menu_unit_kerja_sunting(request, id_unit):
    data_sunting = UnitKerja.objects.get(pk=id_unit)
    if request.method == 'POST':
        form = UnitKerjaForm(request.POST, instance=data_sunting)
        if form.is_valid():
            data_sunting.nama_unit_kerja = form.cleaned_data['nama_unit_kerja']
            data_sunting.save()
            messages.add_message(request, messages.SUCCESS,
                                 "Data %s berhasil diperbarui." % data_sunting.nama_unit_kerja)
            return HttpResponseRedirect(reverse('spk.views.menu_unit_kerja'))
        else:
            return render(request, 'menu-unit-kerja-form.html', {'form': form, 'button': 'Perbarui'})
    form = UnitKerjaForm(instance=data_sunting)
    return render(request, 'menu-unit-kerja-form.html', {'form': form, 'button': 'Perbarui'})


@login_required(login_url='/401')
def json_unit_kerja(request, id_unit):
    unit_kerja = Karyawan.objects.filter(unit_kerja__exact=UnitKerja.objects.get(pk=id_unit)).values_list('nama',
                                                                                                          flat=True)
    return HttpResponse(json.dumps([unicode(i) for i in unit_kerja]), content_type="application/json")


@login_required(login_url='/401')
def menu_perangkingan_promete(request):
    if request.method == 'POST':
        pengetahuan_mesin = [int(i) for i in request.POST.getlist('pengetahuan_mesin')]
        kondisi_psikis = [int(i) for i in request.POST.getlist('kondisi_psikis')]
        kondisi_mesin = [int(i) for i in request.POST.getlist('kondisi_mesin')]
        kondisi_lingkungan = [int(i) for i in request.POST.getlist('kondisi_lingkungan')]
        kondisi_fisik = [int(i) for i in request.POST.getlist('kondisi_fisik')]
        masa_kerja = [int(i) for i in request.POST.getlist('masa_kerja')]
        pendidikan_karyawan = [int(i) for i in request.POST.getlist('pendidikan_karyawan')]
        usia_karyawan = [int(i) for i in request.POST.getlist('usia_karyawan')]
        promet.kriteria_biasa(1, 2)
    return render(request, 'menu-ranking-promethee.html', {'unit_kerja': UnitKerja.objects.all()})