from django.db import models


class Pengguna(models.Model):
    LEVEL_CHOICES = (
        ('Admin', 'Admin'),
        ('K3', 'K3'),
        ('Personalia', 'Personalia'),
    )
    user_nama = models.CharField(max_length=80, verbose_name='Nama Pengguna')
    user_username = models.CharField(max_length=16, verbose_name='Username')
    user_password = models.CharField(max_length=20, verbose_name='Password')
    user_level = models.CharField(max_length=50, verbose_name='Level', choices=LEVEL_CHOICES)

    def __unicode__(self):
        return self.user_nama


class UnitKerja(models.Model):
    nama_unit_kerja = models.CharField(max_length=40)

    def __unicode__(self):
        return self.nama_unit_kerja


class Karyawan(models.Model):
    JENIS_KELAMIN = (
        ('Laki-laki', 'Laki-laki'),
        ('Perempuan', 'Perempuan'),
    )
    nik = models.CharField(max_length=10, primary_key=True)
    nama = models.CharField(max_length=80)
    jenis_kelamin = models.CharField(max_length=15, choices=JENIS_KELAMIN)
    tanggal_masuk = models.DateField()
    unit_kerja = models.ForeignKey(UnitKerja)
    foto = models.ImageField(upload_to='uploads/%Y/%m/%d')

    def __unicode__(self):
        return self.nik


class Laporan(models.Model):
    nilai_netflow = models.FloatField()
    ranking = models.IntegerField()
    karyawan = models.ForeignKey(Karyawan)

    def __unicode__(self):
        return self.karyawan.nik


class Kriteria(models.Model):
    TIPE_PREFERENSI = (
        ('I', 'I - Biasa'),
        ('II', 'II - Quasi'),
        ('III', 'III - Linier'),
    )
    MAX_MIN = (
        ('max', 'Maximum'),
        ('min', 'Minimum'),
    )
    nama_kriteria = models.CharField(max_length=40)
    tipe_preferensi = models.CharField(max_length=5, choices=TIPE_PREFERENSI)
    max_min = models.CharField(max_length=10, choices=MAX_MIN)
    parameter_p = models.IntegerField()
    parameter_q = models.IntegerField()
    nilai_karyawan = models.ManyToManyField(Karyawan, through='NilaiKaryawan')

    def __unicode__(self):
        return self.nama_kriteria


class BobotKriteria(models.Model):
    bobot = models.FloatField()
    kriteria = models.ForeignKey(Kriteria)

    def __unicode__(self):
        return self.kriteria.nama_kriteria


class NilaiKaryawan(models.Model):
    nilai = models.IntegerField()
    karyawan = models.ForeignKey(Karyawan)
    kriteria = models.ForeignKey(Kriteria)

    def __unicode__(self):
        return self.karyawan.nama