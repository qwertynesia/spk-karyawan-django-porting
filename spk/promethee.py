def kriteria_biasa(a, b):
    """
    Preferensi I atau biasa

    Bernilai 0, jika d bernilai 0
    Bernilai 1, jika d tidak bernilai 0

    a : orang pertama
    b : orang kedua
    """
    d = a - b
    return 0 if d == 0 else 1


def kriteria_quasi(a, b, q):
    """
    Preferensi II atau quasi

    Bernilai 0, jika d >= -q dan d <= q
    Bernilai 1, jika d < -q atau d > q

    a : orang pertama
    b : orang kedua
    q : nilai Q pada preferensi Quasi
    """
    d = a - b
    quasi = lambda selisih, arg: (-arg <= selisih <= arg and 0) or (selisih < -arg or selisih > arg and 1)
    return quasi(d, q)


def kriteria_linier(a, b, p):
    """
    Preferensi III atau linier

    Bernilai d/p, jika d >= -p dan d <= p
    Bernilai 1, jika d < -p atau d > p

    a : orang pertama
    b : orang kedua
    p : nilai P pada preferensi linier
    """
    d = a - b
    linier = lambda selisih, arg: (-arg <= selisih <= arg and float(selisih) / float(arg)) or \
                                  (selisih < -arg or selisih > arg and 1)
    return linier(d, p)